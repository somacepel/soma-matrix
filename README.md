# SOMA-MATRIX #

### Para que serve este repositorio ? ###

Este repositório permite registrar os tickets (incidentes) relacionados a 
gestão de mudança no sistema SOMA implantado em Itaipu no escopo do 
projeto MATRIX. Os incidentes podem ser de diversos tipos, tais como,

* Erros de sistema (bugs)
* Sugestões de melhorias
* Dúvidas quanto ao funcionamento de uma dada funcionalidade.
* Colaborações em geral

Usamos para isso um repositório privado do Bitbucket Cloud, que disponibiliza um rastreador de problemas. 
Assim podemos rastrear as solicitações de recursos do seu projeto, relatórios de bugs e outras tarefas 
de gerenciamento de mudanças em projetos de software. Trata-se de um rastreador de problemas simples 
e flexível. Ele tem apenas alguns campos configuráveis (componente, versão e marco).

### Como posso usá-lo ? ###

Para usar é simples. Acesse a URL [Incidentes](https://bitbucket.org/somacepel/soma-matrix/issues)

Veja na imagem abaixo uma lista de incidentes registrados e como interpretar o resultado apresentado.

![Incidentes Bitbucket](https://bitbucket.org/somacepel/soma-matrix/raw/9cf5c4627bf16b0552c5159c95685e52c06d0cfe/incidentesBitbucket.png)

A página que lista os incidentes é repleta de hiperlink para navegar no sistema e todos são auto explicativos.

### Guia para registrar um incidente ###

* Anote o titulo e a descrição do incidente
* Anote endereços IP, URLs, versões de software e outros detalhes que sejam úteis a solução e encaminhamento do incidente.
* Classifique segundo a prioridade (trivial, minor, major, critical, blocker)
* Classifique segundo o tipo de incidente (Bug, Tarefa, Melhoria, Proposta)
* Adicione imagens e anexos que possam ser úteis a resolução do incidente

### Partes interessadas nestes incidentes ###

* Gestão do projeto por parte do CEPEL
* Gestão do projeto por parte de Itaipú
* Equipes de engenharia de manutenção de Itaipú envolvida no projeto
* Equipes de eletrônica, TI, TC e automação de Itaipú envolvida no projeto
* Equipes do PTI (BR/PY) que fazem parte do desenvolvimento do projeto SOMA-MATRIX
* Desenvolvedores do SOMA localizados no Rio de Janeiro
* Equipe de ATENAS ENERGIA S.A. envolvida no projeto
